**Name**

*Add short name of the test case*

Example 1: Configure Foo

Example 2: Login to Bar


**Description**

*Add description of test case*

Example 1:
Verify that Foo can be configured.

Example 2:
Verify that login to Bar is successful.

**Sequence**

*Add test sequence*

Example 1:
1. Open configuration wizard
2. Add configuration
3. Save

Example 1:
1. Run application
2. Login to Bar

**Expected Result**

*Add expected result of test case*

Example 1:
Foo is configured.

Example 2:
Login to Bar is successful.

/label ~"Test Case"
